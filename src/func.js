const getSum = (str1, str2) => {
  const isString1 = typeof str1 !== 'string' || str1.match(/[a-z]/gi);
  const isString2 = typeof str2 !== 'string' || str2.match(/[a-z]/gi);
  if (isString1 || isString2) {
    return false
  }

  return (+str1 + +str2) + '';
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  const result = {
    Post: 0,
    comments: 0,
    toString() {
      return `Post:${this.Post},comments:${this.comments}`
    }
  };

  for (let post of listOfPosts) {
    if (post.author === authorName) {
      result.Post++;
    }

    if (post.comments) {
      for (let comment of post.comments) {
        if (comment.author === authorName) {
          result.comments++;
        }
      }
    }
  }

  return result.toString();
};

const tickets=(people)=> {
  let cash = 0;

  for (let money of people) {
    let gotChange = cash - (money - 25) >= 0;

    if (!gotChange) return 'NO';  

    cash = cash + 25;
  }

  return 'YES';
};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
